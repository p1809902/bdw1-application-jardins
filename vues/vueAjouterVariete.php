<<<<<<< HEAD
<!DOCTYPE html>
<html>
	<head>

		<title><?= $nomSite ?></title>
		<meta charset="utf-8"/>
		<link href="css/style.css" rel="stylesheet" media="all" type="text/css">
		<link rel="shortcut icon" type="image/x-icon" href="img/favicon.png" />
		
	</head>

	<body>

		<?php include('statics/header.php'); ?>
		<?php include('statics/nav.php'); ?>

		<main>

			<h1>Ajout d'une Variété</h1>

			<form method="post" action="#">
				<div class="disp">
				<p>Semencier :
				<br/><span class="myarrow">
				<input class="inputsemencier" list="listenom" name="Semencier" value="Gilbert" autocomplete="off" required>
				</span>
					<?php
					if(isset($_POST['boutonajouter'])) {

						if(verifliste($connexion,$_POST['Semencier'],'nom','Semencier')==0){
							echo "<style>.inputsemencier{border-color:red;}</style>";
							echo "<p style='color:red;'>Veuillez sélectionner une des valeurs proposées pour le Semencier !</p>";
						}
					}
					?>
				<!-- permet de créer la liste des instances stockées en base-->
				<datalist id="listenom"> 
					<?php liste($connexion,'Nom','Semencier'); ?>
				</datalist>
				</p>
				<br/>


				<p>Nom de la Plante :
				<br/>
				<span class="myarrow">
				<input class="inputplante" list="listeplante" name="NomPlante" value="Haricot" autocomplete="off" required>
				</span>
					<?php
						if(isset($_POST['boutonajouter'])) {

							if(verifliste($connexion,$_POST['NomPlante'],'NomPlante','Plante')==0){
								echo "<style>.inputplante{border-color:red;}</style>";
								echo "<p style='color:red;'>Veuillez sélectionner une des valeurs proposées pour la Plante !</p>";
							}
						}
					?>
				<!-- permet de créer la liste des instances stockées en base-->
				<datalist id="listeplante"> 
					<?php liste($connexion,'NomPlante','Plante'); ?>
				</datalist>
				</p>
				<br/>
			
				<p>Type de sol :
				<br/>
				<span class="myarrow">
				<input class="inputsol" list="listesol" name="type_sol" value="terre tourbeuse" autocomplete="off" required>
				</span>
					<?php
						if(isset($_POST['boutonajouter'])) {

							if(verifliste($connexion,$_POST['type_sol'],'Type_sol','type_sol')==0){
								echo "<style>.inputsol{border-color:red;}</style>";
								echo "<p style='display:fixed;color:red;'>Veuillez sélectionner une des valeurs proposées pour le type de sol !</p>";
							}
						}
					?>
				<!-- permet de créer la liste des instances stockées en base-->
				<datalist id="listesol"> 
					<?php liste($connexion,'Type_sol','type_sol'); ?>
				</datalist>
				</p>
			
				<br/>

				<p>Version :
				<br/>
				<select name="version" size="1">
					<option>Biologique
					<option>Conventionnelle
				</select>
				</p>
				<br/>

				<p>Nom de la Variété :
				<br/>
				<input type="text" name="NomVariété" value="Williams" autocomplete="off" required>
				</p>
				<br/>
			
				<p>Année de mise sur le marché :
				<br/>
				<input type="number" min="0" name="AnnéeMarché" autocomplete="off" value="2015" >
				</p>
				<br/>
				
				<p>Code de précocité :
				<br/>
				<input type="text" name="Précocité" autocomplete="off" value="G5" >
				</p>
				<br/>
				
				<p>Description du semis :
				<br/>
				<input type="text" name="DescriptionSemis" value="Semis en ligne" autocomplete="off" >
				</p>
				<br/>
				
				<p>Plantation :
				<br/>
				<input type="text" name="Plantation" value="fruitière" autocomplete="off">
				</p>
				<br/>
			
				<p>Entretien :
				<br/>
				<input type="text" name="Entretien" value="Arrosage régulier" autocomplete="off">
				</p>
				<br/>
				
				<p>Nombre de jour pour la levée :
				<br/>
				<input type="number" min="1" name="NombreJourlevée" value="10" autocomplete="off">
				</p>
				<br/>
				
				<p>Période de mise en place :
				<br/>
				<input type="datetime-local" name="PériodeMisePlace" value="Printemps" autocomplete="off">
				</p>
				<br/>
			
				<p>Période de Récolte :
				<br/>
				<input type="datetime-local" name="PériodeRécolte" value="Automne" autocomplete="off">
				</p>
				<br/>
				
				<p>Commentaire :
				<br/>
				<input type="text" name="Commentaire" value="croquante" autocomplete="off">
				</p>
				<br/>

				<input class="input" type="submit" name="boutonajouter" value="Ajouter" >
				</p>
				</div>
				</br></br>
					<!-- même conditon que sur le contrôleur-->
					<?php 
						if(isset($_POST['boutonajouter'])){ 
							if(verifliste($connexion,$_POST['NomPlante'],'NomPlante','Plante')!=0 and
							verifliste($connexion,$_POST['Semencier'],'nom','Semencier')!=0 and verifliste($connexion,$_POST['type_sol'],'Type_sol','type_sol')!=0){
								echo "<style>.disp{display:none;}</style>";
								echo $info;
					?>
								
								</br></br>
								<!-- permet de recharger la page sans utiliser le menu-->
								<input class="input" type="submit" name="boutonreset" value="reset">
					<?php
							}
						}
					?>
				
		</main>
						

		<?php include('statics/footer.php'); ?>

	</body>


=======
<!DOCTYPE html>
<html>
<head>

	<title><?= $nomSite ?></title>
	<meta charset="utf-8"/>
	<link href="css/style.css" rel="stylesheet" media="all" type="text/css">
	<link rel="shortcut icon" type="image/x-icon" href="img/favicon.png" />
	
</head>

<body>

	<?php include('statics/header.php'); ?>
	<?php include('statics/nav.php'); ?>
	
	<main>

	<h1>Ajout d'une Variété</h1>

	<form method="post" action="#">
	
		<p>Semencier :
		<br/>
		<input class="inputsemencier" list="listenom" name="Semencier" value="Gilbert" required>
			<?php
			if(isset($_POST['boutonajouter'])) {

				if(verifliste($connexion,$_POST['Semencier'],'nom','Semencier')==0){
				echo "<style>.inputsemencier{border-color:red;}</style>";
				echo "<p style='color:red;'>Veuillez sélectionner une des valeurs proposées pour le Semencier !</p>";
				}
			}
			?>
		<datalist id="listenom"> 
			<?php liste($connexion,'Nom','Semencier'); ?>
		</datalist>
		</p>
		<br/>


		<p>Nom de la Plante :
		<br/>
		<input class="inputplante" list="listeplante" name="NomPlante" value="Haricot" required>
			<?php
				if(isset($_POST['boutonajouter'])) {

					if(verifliste($connexion,$_POST['NomPlante'],'NomPlante','Plante')==0){
						echo "<style>.inputplante{border-color:red;}</style>";
						echo "<p style='color:red;'>Veuillez sélectionner une des valeurs proposées pour la Plante !</p>";
					}
				}
			?>
		<datalist id="listeplante"> 
			<?php liste($connexion,'NomPlante','Plante'); ?>
		</datalist>
		</p>
		<br/>
		
		<p>Type de sol :
		<br/>
		<input class="inputsol" list="listesol" name="type_sol" value="terre tourbeuse"  required>
			<?php
				if(isset($_POST['boutonajouter'])) {

					if(verifliste($connexion,$_POST['type_sol'],'Type_sol','type_sol')==0){
						echo "<style>.inputsol{border-color:red;}</style>";
						echo "<p style='display:fixed;color:red;'>Veuillez sélectionner une des valeurs proposées pour le type de sol !</p>";
					}
				}
			?>
		<datalist id="listesol"> 
			<?php liste($connexion,'Type_sol','type_sol'); ?>
		</datalist>
		</p>
		
		<br/>

		<p>Nom de la Variété :
		<br/>
		<input type="text" name="NomVariété" value="Williams" required>
		</p>
		<br/>
		
		<p>Année de mise sur le marché :
		<br/>
		<input type="number" min="0" name="AnnéeMarché" value="2015" >
		</p>
		<br/>
		
		<p>Code de précocité :
		<br/>
		<input type="text" name="Précocité" value="G5" >
		</p>
		<br/>
		
		<p>Description du semis :
		<br/>
		<input type="text" name="DescriptionSemis" value="Semis en ligne" >
		</p>
		<br/>
		
		<p>Plantation :
		<br/>
		<input type="text" name="Plantation" value="fruitière" >
		</p>
		<br/>
		
		<p>Entretien :
		<br/>
		<input type="text" name="Entretien" value="Arrosage régulier" >
		</p>
		<br/>
		
		<p>Nombre de jour pour la levée :
		<br/>
		<input type="number" min="0" name="NombreJourlevée" value="10" >
		</p>
		<br/>
		
		<p>Période de mise en place :
		<br/>
		<input type="text" name="PériodeMisePlace" value="Printemps">
		</p>
		<br/>
		
		<p>Période de Récolte :
		<br/>
		<input type="text" name="PériodeRécolte" value="Automne" >
		</p>
		<br/>
		
		<p>Commentaire :
		<br/>
		<input type="text" name="Commentaire" value="croquante" >
		</p>
		<br/>

		<input type="submit" name="boutonajouter" value="Ajouter">
		</p>
		</br></br>

			<?php if(isset($_POST['boutonajouter'])){ 
			if(verifliste($connexion,$_POST['NomPlante'],'NomPlante','Plante')!=0 and
			verifliste($connexion,$_POST['Semencier'],'nom','Semencier')!=0 and verifliste($connexion,$_POST['type_sol'],'Type_sol','type_sol')!=0){
			echo $info;}}?>
			
	</main>
					

	<?php include('statics/footer.php'); ?>

</body>

>>>>>>> ad3b0f6bd764658e27df8c6975308bd45c561275
</html>
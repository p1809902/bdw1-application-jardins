<!DOCTYPE html>
<html>
	<head>
		<title><?= $nomSite ?></title>
		<meta charset="utf-8"/>
		<link href="css/style.css" rel="stylesheet" media="all" type="text/css">
		<link rel="shortcut icon" type="image/x-icon" href="img/favicon.png" />
	</head>
	<body>

		<?php include('statics/header.php'); ?>
		<?php include('statics/nav.php'); ?>
	
		<main>
			<form method="post" action="#">
			<!-- on simule des boutons pour choisir la fonction future -->
			<div class="divAPropos">
			<input class="divjardin" type="submit" name="voirjardin" value="Afficher un jardin">
			
			<input class="divjardin" type="submit" name="genérerjardin" value="Générer un jardin aléatoirement">
			</div>

			
			<!-- puis on cache les deux "boutons" après le choix -->
			<?php
			if(isset($_POST['voirjardin'])) {
				echo "<style>.divAPropos{display:none;}</style>";
				
			?>
			
		
			<!-- choix 1  -->
			<br/><br/>
			<p>Choissisez le jardin à afficher ! :
			<br/><br/>
			<span class="myarrow">
			<input class="inputjardin" list="listejardin" name="jardin" autocomplete="off" >
			</span>
			<datalist id="listejardin"> 
			<?php liste($connexion,'NomJardin','Jardin'); ?>
			</datalist>
			</p>
			<br/>
			<input class="input" type="submit" name="boutonjardin" value="Jardiner">
			<br/><br/>
			
			<!-- choix 2			-->
			<?php 
			
			}
		
			if(isset($_POST['genérerjardin'])) {
				echo "<style>.divAPropos{display:none;}</style></br></br>";
				jardinAleatoire($connexion,$nb_parcelle);
				
			}
			?>
			
			
			
			
			
			<!-- On affiche directement le choix 1 si il y a eu une erreur dans le choix du jardin (évite de repasser par le menu et le "bouton"  -->
			<?php
			
				if(isset($_POST['boutonjardin'])) {
					echo "<style>.divAPropos{display:none;}</style>";
					if(verifliste($connexion,$nom_jardin,'NomJardin','Jardin')==0){
						
						
			?>
			<br/><br/>
			<p>Choissisez le jardin à afficher ! :
			<br/><br/>
			<span class="myarrow">
			<input class="inputjardin" list="listejardin" name="jardin" autocomplete="off" >
			</span>
			<datalist id="listejardin"> 
			<?php liste($connexion,'NomJardin','Jardin'); ?>
			</datalist>
			</p>
			<br/>
			<input class="input" type="submit" name="boutonjardin" value="Jardiner">
			<br/><br/>
			
			<?php
						echo $info;
					}
				
					if(verifliste($connexion,$nom_jardin,'NomJardin','Jardin')==0){
						echo "<style>.inputjardin{border-color:red;}</style>";
					}
			
					else{
						infojardin($connexion,$nom_jardin);
						afficherjardin($connexion,$nom_jardin);
					}
				}
			
					
					
						
				?>
		</main>
			
		<?php include('statics/footer.php'); ?>

	</body>
</html>
<!DOCTYPE html>
<html>
<head>
	<title><?= $nomSite ?></title>
	<meta charset="utf-8"/>
	<link href="css/style.css" rel="stylesheet" media="all" type="text/css">
	<link rel="shortcut icon" type="image/x-icon" href="img/favicon.png" />
</head>
<body>

<?php include('statics/header.php'); ?>
<?php include('statics/nav.php'); ?>


<h1 class="titreSection">A PROPOS</h1>
		<div class="divAPropos">
	
			<div class="divGauche">
				<h1 class="titrePropos">LE BUT</h1>
				<p>Le but de cette Application est de pouvoir gérer un jardin de manière optimal et ainsi pouvoir gérer ce que l'on fait pousser,
					A quelle saison, de savoir quand la récolte à lieu, etc...
				</p>
			</div>

			<div class="divCentre">
				<h1 class="titrePropos">POUR QUI ?</h1>
				<p>Cette Application est faite pour tout les particuliers et professionnels qui souhaite avoir un moyen de gestion simple et efficace 
					pour leur jardin et ainsi permettre une gestion pérenne au niveau de leur jardin.
				</p>
			</div>

		</div>
    <?php include('statics/footer.php'); ?>

</body>
</html>
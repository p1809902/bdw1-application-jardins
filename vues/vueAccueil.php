<!DOCTYPE html>
<html>
	<head>
		<title><?= $nomSite ?></title>
		<meta charset="utf-8"/>
		<link href="css/style.css" rel="stylesheet" media="all" type="text/css">
		<link rel="shortcut icon" type="image/x-icon" href="img/favicon.png" />
	</head>

	<body>
		<?php include('statics/header.php'); ?>
		<?php include('statics/nav.php'); ?>

		<main>
			<!-- permet d'afficher dynamiquement (avec js) un ensemble de mot prédéfinit -->
			<div class="divPhoto">
				<h1 class="TitreDyn"><span id="word"> EDEN </span></h1>
				<img src="img/img_accueil.jpg" class="img_accueil">
			</div>
			
		</main>
	</body>

	<?php include('statics/footer.php'); ?>	
</html>

<!DOCTYPE html>
<html>
<head>
	<title><?= $nomSite ?></title>
	<meta charset="utf-8"/>
	<link href="css/style.css" rel="stylesheet" media="all" type="text/css">
	<link rel="shortcut icon" type="image/x-icon" href="img/favicon.png" />
</head>
<body>

	<?php include('statics/header.php'); ?>
	<?php include('statics/nav.php'); ?>
	<main>
		 
		<form method="post" action="#">
			<p>Nombre de parcelles : </p>
			<input type="number" min="1" name="nbparcelle" value="2" required>
			<br/><br/>
			<input type="submit" name="boutoncreer" value="Créer" required>
			<br/><br/>


			<?php if(isset($_POST['boutoncreer'])) {
				for($i=0;$i<$_POST['nbparcelle'];$i++){
				parcelle($connexion,2,2,0);}
			}?>
	</main>
		
	<?php include('statics/footer.php'); ?>

</body>

</html>

//permet d'avoir un affichage dynamique sur l'écran d'accueil
$(function(){
	var words = [
		'JARDINEZ',
		'CULTIVEZ',
		'GEREZ',
		'VOYEZ LE JARDINAGE AUTREMENT',
	],
	i = 0;

	setInterval(function() {
		$("#word").fadeOut(function() {
			$(this).html(words[i = (i+1) % words.length]).fadeIn();
		});
	}, 3500);
});

//permet d'afficher la div #stats en appuyant soit sur la flèche soit sur 'afficher les stats'
$('#button').click(function() {
    $('#stats').slideToggle();
	document.getElementById("stats").style.display = "flex";  
})

$('#arrow').click(function() {
    $('#stats').slideToggle();
	document.getElementById("stats").style.display = "flex";  
})
//

//permet de faire une rotation de la flèche des statistique en appuyant soit sur la flèche soit sur 'afficher les stats'
$("#button").click(function(){
	$("#arrow").toggleClass("rotate");
});

$("#arrow").click(function(){
	$(this).toggleClass("rotate");
});
//
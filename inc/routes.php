<?php
//permet de définir les controleurs et les vues à appeler pour l'index
    $routes = array(
            'accueil' => array('controleurs' => 'controleurAccueil', 'vues' => 'vueAccueil'),
            'afficherDonnee' => array('controleurs' => 'controleurDonnee', 'vues' => 'vueDonnee'),
            'ajouterVariete' => array('controleurs' => 'controleurAjouterVariete', 'vues' => 'vueAjouterVariete'),
            'genererParcelle' => array('controleurs' => 'controleurGenererParcelle', 'vues' => 'vueGenererParcelle'),
            'Jardin' => array('controleurs' => 'controleurJardin', 'vues' => 'vueJardin'),
			'a_propos' => array('controleurs' => 'controleurAPropos', 'vues' => 'vueAPropos'),
        );

?>
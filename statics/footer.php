<!-- affichage du "footer" statique --> 
<footer>
	<div id="footer">
	<span>Copyright Lyes OURRED et Clement BOUILLOT</span>
	<span><?php print date("Y"); ?> - <em><?= $nomSite ?></em></span>
	<span><a href="http://liris.cnrs.fr/~fduchate/BDW1/" target="_blank" alt="Page BDW1">BDW1</a></span>
	<span><a href="https://www.univ-lyon1.fr/" alt="UCBL">UCB LYON 1</a></span>
	<span>Remerciements: (<a href="https://www.flaticon.com/">FlatIcon</a>, <a href="https://perso.liris.cnrs.fr/fabien.duchateau/">Fabien Duchateau)</a></span>
</div>
</footer>


<!-- Permet d'avoir une musique d'ambiance sans contrôle (car non personnalisable sans js) --> 
<audio autoplay loop>
		<source src="audio/ambiance.ogg" type="audio/ogg">
        <source src="audio/ambiance.mp3" type="audio/mpeg">
		Your browser does not support the audio element.
</audio>
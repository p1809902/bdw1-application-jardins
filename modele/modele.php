<?php

// connexion à la BD, retourne un lien de connexion
function getConnexionBD() {
	$connexion = mysqli_connect(SERVEUR, UTILISATRICE, MOTDEPASSE, BDD);
	mysqli_set_charset($connexion, "utf8");
	if (mysqli_connect_errno()) {
	    printf("Échec de la connexion : %s\n", mysqli_connect_error());
	    exit();
	}
	return $connexion;
}

//fonction pour afficher le résultat d'une requête sql en tableau (reprise d'un tp)
function printResults($resultats) {
	
	if(mysqli_num_rows($resultats) == 0) 
		echo '<p class="bg-info"> Aucun résultat demandé donc aucun résultat affiché ! :D .</p>';
	else { 
		echo '<table><thead><tr>';
		$fields = mysqli_fetch_fields($resultats);
		foreach ($fields as $field)
			printf("<th>%s</th>", $field->name);
		echo "</tr></thead><tbody>";
		while ($row = mysqli_fetch_row($resultats)) { 
			echo "<tr>";
			foreach ($row as $val) 
				printf("<td>%s</td>", $val); 
			echo "</tr>";
		}
		echo "</tbody></table>";
	}
}



// déconnexion de la BD
function deconnectBD($connexion) {
	mysqli_close($connexion);
}

// vérifie si la donnée entrée est bien dans la datalist proposée
function verifliste($connexion,$input,$nomcol,$nomtable){
	
	$input=mysqli_real_escape_string($connexion,$input);
	$req="SELECT count($nomcol) FROM $nomtable WHERE $nomcol='$input'";
	$res=mysqli_fetch_all(mysqli_query($connexion,$req))[0][0];
    return $res;
}

// Permet d'importer dans la datalist toutes les valeurs de la table et de la colonne choisie
function liste($connexion,$nomcol,$nomtable){
	$req="SELECT $nomcol FROM $nomtable";
    $tab=mysqli_fetch_all(mysqli_query($connexion,$req));
	foreach($tab as $val){
	echo '<option value="'.$val[0].'">'.$val[0].'</option>';
	}
}

// Permet de chercher l'id d'une plante dans la base de données
function GetIdPlante($connexion,$NomPlante){
	$NomPlante=mysqli_real_escape_string($connexion,$NomPlante);
	return mysqli_fetch_all(mysqli_query($connexion,"select id_plante from Plante where NomPlante='$NomPlante'"));
}

// Permet de chercher le si le nom de la variété existe déjà dans la table
function GetNomVariete($connexion,$NomPlante,$idplante){
	$NomPlante=mysqli_real_escape_string($connexion,$NomPlante);
	return mysqli_fetch_all(mysqli_query($connexion,"select NomVariété from Variété where NomVariété='$NomPlante' and id_plante=$idplante"));
}

// Permet d'insérer une variété dans la base de donnée
function InsererVariete($connexion,$NomVariete,$AnneeMarche,$Precocite,$DescriptionSemis,$Plantation,$Entretien,$NombreJourlevee,$PeriodeMisePlace,$PeriodeRecolte,$Commentaire,$idplante){
	
	// on escape le texte pour éviter les erreurs
	$NomVariete=mysqli_real_escape_string($connexion,$NomVariete);
	$AnneeMarche=mysqli_real_escape_string($connexion,$AnneeMarche);
	$Precocite=mysqli_real_escape_string($connexion,$Precocite);
	$DescriptionSemis=mysqli_real_escape_string($connexion,$DescriptionSemis);
	$Plantation=mysqli_real_escape_string($connexion,$Plantation);
	$Entretien=mysqli_real_escape_string($connexion,$Entretien);
	$NombreJourlevee=mysqli_real_escape_string($connexion,$NombreJourlevee);
	$PeriodeMisePlace=mysqli_real_escape_string($connexion,$PeriodeMisePlace);
	$PeriodeRecolte=mysqli_real_escape_string($connexion,$PeriodeRecolte);
	$Commentaire=mysqli_real_escape_string($connexion,$Commentaire);
	
	$req="Insert into Variété(NomVariété,AnnéeMarché,Précocité,DescriptionSemis,Plantation,Entretien,NombreJourlevée,PériodeMisePlace,PériodeRécolte,Commentaire,id_plante) values('$NomVariete','$AnneeMarche','$Precocite','$DescriptionSemis','$Plantation','$Entretien','$NombreJourlevee','$PeriodeMisePlace','$PeriodeRecolte','$Commentaire','$idplante')";
	
	// On remplace les chaines vides par la valeur NULL
	$req=str_replace("''","NULL",$req);
	
	mysqli_query($connexion,$req);
	
}


// Permet de retourner les données souhaité en fonction d'une limite dictée
function AfficherDonnee($connexion,$limit,$table){
	$limit=mysqli_real_escape_string($connexion,$limit);
	$table=mysqli_real_escape_string($connexion,$table);
	
	//on regarde si on affiche toutes les données ou non
	if($limit=='-1'){
			if($table=='Plante'){
				$req="select NomPlante,catégorie,Nomlatin from $table";
			}
			if($table=='Variété'){
				$req="select Nomvariété,AnnéeMarché,Précocité,DescriptionPrécocité,DescriptionSemis,Plantation,Entretien,NombreJourlevée,PériodeMisePlace,PériodeRécolte,Commentaire from $table";
			}
			if($table=='Type'){
				$req="select type from $table";
			}
			return mysqli_query($connexion,$req);		
	}
	else{
		if($table=='Plante'){
			$req="select NomPlante,catégorie,Nomlatin from $table limit $limit";
		}
		if($table=='Variété'){
			$req="select Nomvariété,AnnéeMarché,Précocité,DescriptionPrécocité,DescriptionSemis,Plantation,Entretien,NombreJourlevée,PériodeMisePlace,PériodeRécolte,Commentaire from $table limit $limit";
		}
		if($table=='Type'){
			$req="select type from $table limit $limit";
		}
		return mysqli_query($connexion,$req);
	}
}

// Permet d'afficher et (mémoriser par référence) une parcelle en tableau html
function affParcelle($connexion,$nb_rg_cult,$nb_rg_ind,$vide,&$parcelles){
	
	echo '<table><thead><tr><th colspan="5">Parcelle</th></tr></thead><tbody>';
	
	//boucle pour le nombre de rang cultivé
	for($i=0;$i<$nb_rg_cult;$i++){
		//On tire un "aléatoirement" si c'est une association de variété ou non
		$nb_plante=rand(1,2);
		
		//On vérifie le tirage
		if($nb_plante==2){
			$requete_plante="SELECT id_plante,id_plante_1 FROM Association_plante ORDER BY RAND() LIMIT 1";
			$res_plante=mysqli_fetch_all(mysqli_query($connexion,$requete_plante),MYSQLI_ASSOC);
			$id_plante_1=$res_plante[0]['id_plante'];
			$id_plante_2=$res_plante[0]['id_plante_1'];
			$requete_var_1="SELECT id_variété,NomVariété FROM Variété where id_plante=$id_plante_1 ORDER BY RAND() LIMIT 1";
			$res_var_1=mysqli_fetch_all(mysqli_query($connexion,$requete_var_1),MYSQLI_ASSOC);
			$requete_var_2="SELECT id_variété,NomVariété FROM Variété where id_plante=$id_plante_2 ORDER BY RAND() LIMIT 1";
			$res_var_2=mysqli_fetch_all(mysqli_query($connexion,$requete_var_2),MYSQLI_ASSOC);
			$nom_variete_1=$res_var_1[0]['NomVariété'];
			$nom_variete_2=$res_var_2[0]['NomVariété'];
			$req_plante_1="SELECT NomPlante FROM Plante where id_plante=$id_plante_1";
			$req_plante_2="SELECT NomPlante FROM Plante where id_plante=$id_plante_2";
			
			$res_plante_1=mysqli_fetch_all(mysqli_query($connexion,$req_plante_1),MYSQLI_ASSOC);
			$res_plante_2=mysqli_fetch_all(mysqli_query($connexion,$req_plante_2),MYSQLI_ASSOC);
			
			echo '<tr><th scope="row">rang cultivé '.($i+1).'</br>(association)</th>';
			echo "<td colspan='2'>Plante : ".$res_plante_1[0]['NomPlante']."</br></br>Variété : ".$nom_variete_1."</td>";
			echo "<td colspan='2'>Plante : ".$res_plante_2[0]['NomPlante']."</br></br>Variété : ".$nom_variete_2."</td>";
			$id_variété_1=$res_var_1[0]['id_variété'];
			$id_variété_2=$res_var_2[0]['id_variété'];
			$infoparcelle=array("id_variété_1"=>$id_variété_1,"id_variété_2"=>$id_variété_2,"id_plante_1"=>$id_plante_1,"id_plante_2"=>$id_plante_2);
		}
		else{
			$requete="SELECT DISTINCT id_plante,id_variété,NomVariété FROM Variété ORDER BY RAND() LIMIT $nb_plante";
			$res=mysqli_fetch_all(mysqli_query($connexion,$requete),MYSQLI_ASSOC);
		
			echo '<tr><th scope="row">rang cultivé '.($i+1).'</th>';
			$id=$res[0]['id_plante'];
			$nomvariete=$res[0]['NomVariété'];
			$req_plante="SELECT NomPlante FROM Plante where id_plante=$id";
			$res_plante=mysqli_fetch_all(mysqli_query($connexion,$req_plante),MYSQLI_ASSOC);
			echo "<td colspan='4'>Plante : ".$res_plante[0]['NomPlante']."</br></br>Variété : ".$nomvariete."</td>";
			$id_variété=$res[0]['id_variété'];
			$infoparcelle=array("id_variété"=>$id_variété);
		}
		echo '</tr>';
		// On mémorise les informations générées (par référence) afin de pouvoir les manipuler.
		$parcelles[]=$infoparcelle;
		
		}
	
	// Nombre de rang avec des plantes indésirables
	for($i=0;$i<$nb_rg_ind;$i++){
		
		$requete="SELECT id_variété,NomVariété,id_plante FROM Variété ORDER BY RAND() LIMIT 1";
		$res=mysqli_fetch_all(mysqli_query($connexion,$requete),MYSQLI_ASSOC);
		echo '<tr><th scope="row">rang Indésirable '.($i+1).'</th>';
		$id=$res[0]['id_plante'];
		$nomvariete=$res[0]['NomVariété'];
		$req_plante="SELECT NomPlante FROM Plante where id_plante=$id";
		$res_plante=mysqli_fetch_all(mysqli_query($connexion,$req_plante),MYSQLI_ASSOC);
		echo "<td colspan='4'>Plante : ".$res_plante[0]['NomPlante']."</br></br>Variété : ".$nomvariete."</td>";
		$id_variété=$res[0]['id_variété'];
		$infoparcelle=array("id_variété"=>$id_variété);
		
		$parcelles[]=$infoparcelle;
		}	
		echo '</tr>';
		
		

	// On regarde si il existe des rangs vide puis on affiche
    if($vide!=0){
		for($i=0;$i<$vide;$i++){
			echo '<tr><th scope="row">rang vide '.($i+1).'</th></tr>';
			$parcelles[]=array("id_variété"=>'NULL');
		}
	}
		
	echo "</tbody></table>";
	
	
}




function getinstance($connexion,$table){
	$req="Select * from $table";
	return count(mysqli_fetch_all(mysqli_query($connexion,$req)));
	}
	
//Permet d'insérer dans la bd les variétés, le rang, la parcelle pour un jardin spécifique
function insertparcelle($connexion,$jardin,$parcelle){
	
	// On ajoute une nouvelle parcelle dans le jardin puis on récupère son id
	mysqli_query($connexion,"insert into Parcelles(Id_Jardin) values($jardin);");
	$id_parcelle=mysqli_insert_id($connexion);
	$numero_rang=1;
	
	foreach($parcelle as $rang){
		// On ajoute une nouveau rang dans la parcelle 
		mysqli_query($connexion,"insert into Rang(id_parcelle,NumRang) values($id_parcelle,$numero_rang);");
		
		//On vérifie si le rang est une association de variété ou non
		if(count($rang)==1){	
		
			//On récupère les informations et on créé une période pour insérer dans la bd
			$id_var=$rang['id_variété'];
			$debut=date("Y-m-d H:i:s");
			$fin=date("Y-m-d H:i:s",mktime(date("H"),date("i"),date("s"),date("m")+1,date("d"),date("Y")));
			mysqli_query($connexion,"insert into période(Période_début,Période_fin) values('$debut','$fin');");
			$req="insert into Récolter(id_parcelle,NumRang,id_variété,Période_début,Période_fin) values($id_parcelle,$numero_rang,$id_var,'$debut','$fin');";
			mysqli_query($connexion,$req);
		}
		
		else{
			//On récupère les informations et on créé une période pour insérer dans la bd
			$id_var_1=$rang['id_variété_1'];
			$id_var_2=$rang['id_variété_2'];
			$debut=date("Y-m-d H:i:s");
			$fin=date("Y-m-d H:i:s",mktime(date("H"),date("i"),date("s"),date("m")+1,date("d"),date("Y")));
			mysqli_query($connexion,"insert into période(Période_début,Période_fin) values('$debut','$fin');");
			$id_plante_1=$rang['id_plante_1'];
			$id_plante_2=$rang['id_plante_2'];
			mysqli_query($connexion,"insert into Association_plante(id_plante,id_plante_1) values($id_plante_1,$id_plante_2);");
			$req="insert into Récolter(id_parcelle,NumRang,id_variété,Période_début,Période_fin) values($id_parcelle,$numero_rang,$id_var_1,'$debut','$fin'),($id_parcelle,$numero_rang,$id_var_2,'$debut','$fin');";
			mysqli_query($connexion,$req);
		}
		$numero_rang+=1;	
	}
}

// Permet de récupérer l'identifiant d'un jardin par rapport à son nom
function getidjardin($connexion,$jardin){
	$nom=mysqli_real_escape_string($connexion,$jardin);
	$req="select Id_Jardin from Jardin where NomJardin='$nom'";
	$res=mysqli_fetch_all(mysqli_query($connexion,$req));
	return $res[0][0];
}

//Permet d'afficher un jardin par rapport à son nom
function afficherjardin($connexion,$nomjardin){
	
	$nom=mysqli_real_escape_string($connexion,$nomjardin);
	$id_jardin=mysqli_fetch_all(mysqli_query($connexion,"SELECT id_jardin FROM Jardin where NomJardin='$nom'"))[0][0]; 
	$nb_max_rang=mysqli_fetch_all(mysqli_query($connexion,"SELECT MAX(nb) as max from (SELECT MAX(NumRang) as nb FROM Récolter NATURAL JOIN Parcelles WHERE id_jardin='$id_jardin' group BY id_parcelle)as b"))[0][0];
	$ALLparcelle=mysqli_fetch_all(mysqli_query($connexion,"SELECT id_parcelle FROM Récolter NATURAL JOIN Parcelles WHERE id_jardin='$id_jardin' group BY id_parcelle"));
	$affiche_parcelle=1;
	// On parcourt chaque identifiant des parcelles du jardin
	foreach($ALLparcelle as $parcelle ){
		$id_parcelle=$parcelle[0];
		$jardin=mysqli_fetch_all(mysqli_query($connexion,"SELECT * FROM Récolter NATURAL JOIN Parcelles where id_jardin='$id_jardin' and id_parcelle='$id_parcelle'"),MYSQLI_ASSOC);
		$nb_rang=mysqli_fetch_all(mysqli_query($connexion,"SELECT max(NumRang) FROM Récolter NATURAL JOIN Parcelles WHERE id_jardin='$id_jardin' and id_parcelle='$id_parcelle' group BY id_parcelle LIMIT 1"))[0][0];
		if($nb_rang==1){$padding_bot=($nb_max_rang-$nb_rang)*75.6+8;}
		else{$padding_bot=($nb_max_rang-$nb_rang)*75.6+8.6;}
		$nb_rang=mysqli_fetch_all(mysqli_query($connexion,"SELECT count(NumRang) FROM Récolter NATURAL JOIN Parcelles WHERE id_jardin='$id_jardin' and id_parcelle='$id_parcelle' group BY id_parcelle LIMIT 1"))[0][0];
		echo "<fieldset class='chemin' style='padding-bottom:$padding_bot"."px;width:276px;'>";
		echo "<table class='tableparcelleimage'><tbody>";
		$i=0;
		
		$affiche_rang=1;
		
		//On parcourt pour une parcelle donnée tous les rangs qui la compose
		while($i<$nb_rang-1){
			//on regarde si il y a une association de variété.
			if($jardin[$i]['NumRang']==$jardin[$i+1]['NumRang']){
				// On récupère toutes les informations nécéssaires pour l'affichage
				$id_variété_1=$jardin[$i]['id_variété'];
				$id_variété_2=$jardin[$i+1]['id_variété'];
				$req_variete_1="SELECT NomVariété,id_plante FROM Variété where id_variété=$id_variété_1";
				$req_variete_2="SELECT NomVariété,id_plante FROM Variété where id_variété=$id_variété_2";
				$nom_variete_1=mysqli_fetch_all(mysqli_query($connexion,$req_variete_1),MYSQLI_ASSOC)[0]['NomVariété'];
				$nom_variete_2=mysqli_fetch_all(mysqli_query($connexion,$req_variete_2),MYSQLI_ASSOC)[0]['NomVariété'];
				$id_plante_1=mysqli_fetch_all(mysqli_query($connexion,$req_variete_1),MYSQLI_ASSOC)[0]['id_plante'];
				$id_plante_2=mysqli_fetch_all(mysqli_query($connexion,$req_variete_2),MYSQLI_ASSOC)[0]['id_plante'];
				$req_plante_1="SELECT NomPlante FROM Plante where id_plante=$id_plante_1";
				$req_plante_2="SELECT NomPlante FROM Plante where id_plante=$id_plante_2";
				$nomplante_1=mysqli_fetch_all(mysqli_query($connexion,$req_plante_1),MYSQLI_ASSOC)[0]['NomPlante'];
				$nomplante_2=mysqli_fetch_all(mysqli_query($connexion,$req_plante_2),MYSQLI_ASSOC)[0]['NomPlante'];
				$image_1=mysqli_fetch_all(mysqli_query($connexion,"SELECT chemin FROM Image where id_plante=$id_plante_1"),MYSQLI_ASSOC)[0]['chemin'];
				$image_2=mysqli_fetch_all(mysqli_query($connexion,"SELECT chemin FROM Image where id_plante=$id_plante_2"),MYSQLI_ASSOC)[0]['chemin'];
				echo '<tr style="height:72.6px;">';
				echo "<td height=10px colspan=2 class='tdparcelleimage'><div class='infobulle'><img width='25%' height='25%' src='$image_1'/><img width='25%' height='25%' src='$image_2'/><span class='infobulle_texte'>(Parcelle : $affiche_parcelle/ Rang : $affiche_rang )<p>Plantes (Association) :</p>$nomplante_1 & $nomplante_2</br><hr><p>Variétés:</p>$nom_variete_1 & $nom_variete_2</span></div></td>";
				$i+=2;
			}
			
			//Sinon on affiche une seule variété dans le rang
			else{
	
				$id_variété=$jardin[$i]['id_variété'];
				$req_variete="SELECT NomVariété,id_plante FROM Variété where id_variété=$id_variété";
				$nom_variete=mysqli_fetch_all(mysqli_query($connexion,$req_variete),MYSQLI_ASSOC)[0]['NomVariété'];
				$id_plante=mysqli_fetch_all(mysqli_query($connexion,$req_variete),MYSQLI_ASSOC)[0]['id_plante'];
				$req_plante="SELECT NomPlante FROM Plante where id_plante=$id_plante";
				$nomplante=mysqli_fetch_all(mysqli_query($connexion,$req_plante),MYSQLI_ASSOC)[0]['NomPlante'];
				$image=mysqli_fetch_all(mysqli_query($connexion,"SELECT chemin FROM Image where id_plante=$id_plante"),MYSQLI_ASSOC)[0]['chemin'];
				echo '<tr style="height:72.6px;">';
				echo "<td height=10px colspan=2 class='tdparcelleimage'><div class='infobulle'><img width='25%' height='25%' src='$image'/><span class='infobulle_texte'>(Parcelle : $affiche_parcelle/ Rang : $affiche_rang )<p>Plante:</p>$nomplante</br><hr><p>Variété:</p>$nom_variete</span></div></td>";
				$i+=1;
			}
			$affiche_rang+=1;
			// on complète la dernière variété oublié par le while (à cause de la comparaison en i+1)
			if($i==$nb_rang-1){
				$id_variété=$jardin[$i]['id_variété'];
				$req_variete="SELECT NomVariété,id_plante FROM Variété where id_variété=$id_variété";
				$nom_variete=mysqli_fetch_all(mysqli_query($connexion,$req_variete),MYSQLI_ASSOC)[0]['NomVariété'];
				$id_plante=mysqli_fetch_all(mysqli_query($connexion,$req_variete),MYSQLI_ASSOC)[0]['id_plante'];
				$req_plante="SELECT NomPlante FROM Plante where id_plante=$id_plante";
				$nomplante=mysqli_fetch_all(mysqli_query($connexion,$req_plante),MYSQLI_ASSOC)[0]['NomPlante'];
				$image=mysqli_fetch_all(mysqli_query($connexion,"SELECT chemin FROM Image where id_plante=$id_plante"),MYSQLI_ASSOC)[0]['chemin'];
				echo '<tr style="height:72.6px;">';
				echo "<td height=10px colspan=2 class='tdparcelleimage'><div class='infobulle'><img width='25%' height='25%' src='$image'/><span class='infobulle_texte'>(Parcelle : $affiche_parcelle/ Rang : $affiche_rang )<p>Plante:</p>$nomplante</br><hr><p>Variété:</p>$nom_variete</span></div></td>";
				
				
			}
			
			echo '</tr>';
			
		}
		$affiche_parcelle+=1;
			echo "</tbody></table>";
			echo "</fieldset>";
	}
	
}
// fonction d'insertion d'information en base	
function insertproduire($connexion,$variété,$semencier,$version,$plante){
	$plante=mysqli_real_escape_string($connexion,$plante);
	$variété=mysqli_real_escape_string($connexion,$variété);
	$semencier=mysqli_real_escape_string($connexion,$semencier);
	$version=mysqli_real_escape_string($connexion,$version);
	$id_plante=mysqli_fetch_all(mysqli_query($connexion,"select id_plante from Plante where NomPlante='$plante'"))[0][0];
	$id_variété=mysqli_fetch_all(mysqli_query($connexion,"select id_variété from Variété where NomVariété='$variété' and id_plante=$id_plante"))[0][0];
	$id_semencier=mysqli_fetch_all(mysqli_query($connexion,"select id_semencier from Semencier where Nom='$semencier'"))[0][0];
	$id_version=mysqli_fetch_all(mysqli_query($connexion,"select id_version from Version where nom_version='$version'"))[0][0];
	$req="insert into Produire values($id_variété,$id_semencier,$id_version)";
	mysqli_query($connexion,$req);
}
// fonction de vérification d'information en base
function verifversion($connexion,$variété,$semencier,$version,$plante){
	$plante=mysqli_real_escape_string($connexion,$plante);
	$variété=mysqli_real_escape_string($connexion,$variété);
	$semencier=mysqli_real_escape_string($connexion,$semencier);
	$version=mysqli_real_escape_string($connexion,$version);
	$id_plante=mysqli_fetch_all(mysqli_query($connexion,"select id_plante from Plante where NomPlante='$plante'"))[0][0];
	$id_variété=mysqli_fetch_all(mysqli_query($connexion,"select id_variété from Variété where NomVariété='$variété' and id_plante=$id_plante"))[0][0];
	$id_semencier=mysqli_fetch_all(mysqli_query($connexion,"select id_semencier from Semencier where Nom='$semencier'"))[0][0];
	$id_version=mysqli_fetch_all(mysqli_query($connexion,"select id_version from Version where nom_version='$version'"))[0][0];
	return mysqli_fetch_all(mysqli_query($connexion,"select * from Produire where id_variété=$id_variété and id_semencier=$id_semencier and id_version=$id_version"));
}
// fonction de récupération d'information en base
function infojardin($connexion,$nomjardin){
	$nom=mysqli_real_escape_string($connexion,$nomjardin);
	$id_jardin=mysqli_fetch_all(mysqli_query($connexion,"SELECT id_jardin FROM Jardin where NomJardin='$nom'"))[0][0]; 
	$nb_max_rang=mysqli_fetch_all(mysqli_query($connexion,"SELECT MAX(nb) as max from (SELECT MAX(NumRang) as nb FROM Récolter NATURAL JOIN Parcelles WHERE id_jardin='$id_jardin' group BY id_parcelle)as b"))[0][0];
	$ALLparcelle=mysqli_fetch_all(mysqli_query($connexion,"SELECT count(id_parcelle) from (SELECT * FROM Récolter NATURAL JOIN Parcelles WHERE id_jardin='$id_jardin' group BY id_parcelle) t"))[0][0];
	$nb_rang=mysqli_fetch_all(mysqli_query($connexion,"SELECT SUM(nb_rang) from (SELECT max(NumRang) as nb_rang FROM Récolter NATURAL JOIN Parcelles WHERE id_jardin='$id_jardin' group by id_parcelle) t"))[0][0];
	if(empty($ALLparcelle)){$ALLparcelle=0;};
	if(empty($nb_rang)){$nb_rang=0;};
	if(empty($nb_max_rang)){$nb_max_rang=0;};
	echo "</br></br><h3 class='infojardin' style='border-bottom:none;text-decoration:underline;'>INFORMATIONS</h3><p class='infojardin'>Nom du jardin : $nom </br></br> Nombre de parcelle : $ALLparcelle </br></br> Nombre total de rang : $nb_rang </br></br> Maximum de rang sur une parcelle : $nb_max_rang</p></br></br>";
}



// Permet de générer un jardin aléatoirement	
function jardinAleatoire($connexion,$nb_parcelle){
	
	$max=1;
	$nb_total_rang=0;
	for($i=0;$i<$nb_parcelle;$i++){
		$rand=rand(1,10);
		$rang[]=array('nbrang'=>$rand);
		$nb_total_rang+=$rand;
		if($max<$rand){
			$max=$rand;
		}
	}
	

	echo "<h3 class='infojardin' style='border-bottom:none;text-decoration:underline;'>INFORMATIONS</h3><p class='infojardin'>Jardin Aléatoire </br></br> Nombre de parcelle : $nb_parcelle </br></br> Nombre total de rang : $nb_total_rang </br></br> Maximum de rang sur une parcelle : $max </br> </p></br>";

		
	
	// On parcourt chaque parcelle
	for($i=0;$i<$nb_parcelle;$i++){
		$nb_rang=$rang[$i]['nbrang'];
		//on définit un padding entre la parcelle la plus grande et celle que l'on crée pour combler l'espace
		if($nb_rang==1){$padding_bot=($max-$nb_rang)*75.6+8;}
		else{$padding_bot=($max-$nb_rang)*75.6+8.6;}
		
		echo "<fieldset class='chemin' style='padding-bottom:$padding_bot"."px;width:276px;'>";
		echo "<table class='tableparcelleimage'><tbody>";
		$j=0;
		
		//On parcourt pour une parcelle donnée tous les rangs qui la compose
		while($j<$nb_rang){
			$assoc=rand(0,4);
			//on regarde si il y a une association de variété.
			if($assoc==0){
				// On récupère toutes les informations nécéssaires pour l'affichage
				$requete_plante="SELECT id_plante,id_plante_1 FROM Association_plante ORDER BY RAND() LIMIT 1";
				$res_plante=mysqli_fetch_all(mysqli_query($connexion,$requete_plante),MYSQLI_ASSOC);
				$id_plante_1=$res_plante[0]['id_plante'];
				$id_plante_2=$res_plante[0]['id_plante_1'];
				
				$requete_var_1="SELECT NomVariété FROM Variété where id_plante=$id_plante_1 ORDER BY RAND() LIMIT 1";
				$res_var_1=mysqli_fetch_all(mysqli_query($connexion,$requete_var_1),MYSQLI_ASSOC);
				$requete_var_2="SELECT NomVariété FROM Variété where id_plante=$id_plante_2 ORDER BY RAND() LIMIT 1";
				$res_var_2=mysqli_fetch_all(mysqli_query($connexion,$requete_var_2),MYSQLI_ASSOC);
				$nom_variete_1=$res_var_1[0]['NomVariété'];
				$nom_variete_2=$res_var_2[0]['NomVariété'];
				
				$req_plante_1="SELECT NomPlante FROM Plante where id_plante=$id_plante_1";
				$req_plante_2="SELECT NomPlante FROM Plante where id_plante=$id_plante_2";
				$nomplante_1=mysqli_fetch_all(mysqli_query($connexion,$req_plante_1),MYSQLI_ASSOC)[0]['NomPlante'];
				$nomplante_2=mysqli_fetch_all(mysqli_query($connexion,$req_plante_2),MYSQLI_ASSOC)[0]['NomPlante'];
				$image_1=mysqli_fetch_all(mysqli_query($connexion,"SELECT chemin FROM Image where id_plante=$id_plante_1"),MYSQLI_ASSOC)[0]['chemin'];
				$image_2=mysqli_fetch_all(mysqli_query($connexion,"SELECT chemin FROM Image where id_plante=$id_plante_2"),MYSQLI_ASSOC)[0]['chemin'];
				echo '<tr style="height:72.6px;">';
				echo "<td height=10px colspan=2 class='tdparcelleimage'><div class='infobulle'><img width='25%' height='25%' src='$image_1'/><img width='25%' height='25%' src='$image_2'/><span class='infobulle_texte'>(Parcelle : ".($i+1)."/ Rang : ".($j+1)." )<p>Plantes (Association) :</p>$nomplante_1 & $nomplante_2</br><hr><p>Variétés:</p>$nom_variete_1 & $nom_variete_2</span></div></td>";
				
			}
			
			//Sinon on affiche une seule variété dans le rang
			else{
				$requete="SELECT distinct id_variété,NomVariété,id_plante FROM Variété ORDER BY RAND() LIMIT 1";
				$res=mysqli_fetch_all(mysqli_query($connexion,$requete),MYSQLI_ASSOC);
				$nom_variete=$res[0]['NomVariété'];
				$id_plante=$res[0]['id_plante'];
				$req_plante="SELECT NomPlante FROM Plante where id_plante=$id_plante";
				$nomplante=mysqli_fetch_all(mysqli_query($connexion,$req_plante),MYSQLI_ASSOC)[0]['NomPlante'];
				$image=mysqli_fetch_all(mysqli_query($connexion,"SELECT chemin FROM Image where id_plante=$id_plante"),MYSQLI_ASSOC)[0]['chemin'];
				echo '<tr style="height:72.6px;">';
				echo "<td height=10px colspan=2 class='tdparcelleimage'><div class='infobulle'><img width='25%' height='25%' src='$image'/><span class='infobulle_texte'>(Parcelle : ".($i+1)."/ Rang : ".($j+1).")<p>Plante:</p>$nomplante</br><hr><p>Variété:</p>$nom_variete</span></div></td>";
				
			}
			$j+=1;
			
			echo '</tr>';
			
		}
			echo "</tbody></table>";
			echo "</fieldset>";
	}
	
}		
// fonction d'insertion d'information en base	
function insertjardin($connexion,$nom){
	$nom_jardin=mysqli_real_escape_string($connexion,$nom);
	$req="insert into Jardin(NomJardin) values ('$nom_jardin')";
	mysqli_query($connexion,$req);
	return mysqli_insert_id($connexion);
}

// Afficher les statistiques 
function stats($connexion){
	$res_plante_plus_utilisé=mysqli_fetch_all(mysqli_query($connexion,"SELECT NbVariete, PlantePlusUtilise FROM (SELECT COUNT(id_variété) as NbVariete,MAX(id_plante) as PlantePlusUtilise
    FROM Variété GROUP BY id_plante) t ORDER BY NbVariete DESC LIMIT 3"),MYSQLI_ASSOC);
	echo "<ol><p> <span>Top 3 des plantes avec le plus de variété : </span></br>";
	
	foreach($res_plante_plus_utilisé as $plante){
		$nom=mysqli_fetch_all(mysqli_query($connexion,"select NomPlante from Plante where id_plante=$plante[PlantePlusUtilise]"))[0][0];
		echo "<li>  $nom </li>";	
	}
	echo "</p></ol>";
	
	$res_jardin=mysqli_fetch_all(mysqli_query($connexion,"SELECT id_jardin,SUM(nb) as nb from (SELECT Id_Jardin,id_parcelle,MAX(NumRang) as nb FROM Récolter NATURAL JOIN Parcelles group BY id_parcelle) u group by Id_Jardin order by nb DESC limit 3"),MYSQLI_ASSOC);
	echo "<ol><p><span> Top 3 des jardins avec le plus de rang: </span></br>";

	foreach($res_jardin as $jardin){
		$nom=mysqli_fetch_all(mysqli_query($connexion,"select NomJardin from Jardin where id_jardin=$jardin[id_jardin]"))[0][0];
		echo "<li>  $nom  ($jardin[nb])</li>";
		
	}
	echo "</p></ol>";

	echo "<p></br> <span>Nombre de Plante :</span></br></br>".getinstance($connexion,"Plante")."</p>";
	echo "<p></br> <span>Nombre de Variété :</span></br></br>".getinstance($connexion,"Variété")."</p>";
	echo "<p></br> <span>Nombre de Type :</span></br></br>".getinstance($connexion,"Type")."</p>";
}
?>
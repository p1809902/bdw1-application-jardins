<?php
define('SERVEUR', 'localhost');
define('UTILISATRICE', 'root'); 
define('MOTDEPASSE', ''); 
define('BDD', 'appli_jardin'); 

function getConnexionBD() {
	$connexion = mysqli_connect(SERVEUR, UTILISATRICE, MOTDEPASSE, BDD);
	mysqli_set_charset($connexion, "utf8");
	if (mysqli_connect_errno()) {
	    printf("Échec de la connexion : %s\n", mysqli_connect_error());
	    exit();
	}
	return $connexion;
}

$connexion=getConnexionBD();

$req="Select * from appli_jardin.donneesfournies ";
$res=mysqli_fetch_all(mysqli_query($connexion,$req),MYSQLI_ASSOC);

//On parcourt les données 

foreach($res as $array){
	
	// on escape les données
	$codeVariété=mysqli_real_escape_string($connexion,$array['codeVariété']);
	$nomEspèce=mysqli_real_escape_string($connexion,$array['nomEspèce']);
	$nomEspèceLatin=mysqli_real_escape_string($connexion,$array['nomEspèceLatin']);
	$commentaire=mysqli_real_escape_string($connexion,$array['commentaire']);
	$codePrécocité=mysqli_real_escape_string($connexion,$array['codePrécocité']);
	$labelPrécocité=mysqli_real_escape_string($connexion,$array['labelPrécocité']);
	$annéeEnregistrement=mysqli_real_escape_string($connexion,$array['annéeEnregistrement']);
	$type=mysqli_real_escape_string($connexion,$array['type']);
	$sousType=mysqli_real_escape_string($connexion,$array['sousType']);
	
	
	// On réalise les insertions en remplaçant les chaines vides par null
	mysqli_query($connexion,str_replace("''","NULL","Insert into Type(type) select t.type From (Select '$type' as type) t WHERE NOT EXISTS(select * from Type where type=t.type)"));
	
	$id_type=mysqli_fetch_all(mysqli_query($connexion,"select id_type from Type where type='$type'"))[0][0];
	
	mysqli_query($connexion,str_replace("''","NULL","Insert into Sous_type(id_type,sous_type) select t.id,t.stype From (Select $id_type as id,'$sousType' as stype) t WHERE NOT EXISTS(select * from Sous_type where sous_type=t.stype or sous_type is NULL and t.stype is NULL)"));
	
	$id_sous_type=mysqli_fetch_all(mysqli_query($connexion,"select id_sous_type from Sous_type where sous_type='$sousType' or sous_type is NULL"))[0][0];
	
	mysqli_query($connexion,str_replace("''","NULL","Insert into Plante(id_sous_type,NomPlante,Nomlatin) select t.id,t.np,t.nl from (select $id_sous_type as id,'$nomEspèce' as np,'$nomEspèceLatin' as nl) t WHERE NOT EXISTS(select NomPlante from Plante where NomPlante=t.np)"));
   
    $id_plante=mysqli_fetch_all(mysqli_query($connexion,"select id_plante from Plante where NomPlante='$nomEspèce'"))[0][0];
	
	mysqli_query($connexion,str_replace("''","NULL","Insert into Variété(id_plante,NomVariété,Commentaire,Précocité,DescriptionPrécocité,AnnéeMarché) select t.id,t.nv,t.c,t.p,t.lp,t.a from (select $id_plante as id,'$codeVariété' as nv,'$commentaire' as c,'$codePrécocité' as p,'$labelPrécocité' as lp,'$annéeEnregistrement' as a) t where not exists( select * from Variété where id_plante=t.id and NomVariété=t.nv)"));

}

// insertion des images/Période/association/récolte après l'intégration (contrainte clé étrangère)
mysqli_query($connexion,
'INSERT INTO Image(id_plante,chemin)
values
(1,"img/peach.png"),(107,"img/peach.png"),
(2,"img/wheat.png"),(47,"img/wheat.png"),(15,"img/wheat.png"),(46,"img/wheat.png"),
(3,"img/cherries.png"),(151,"img/cherries.png"),(130,"img/cherries.png"),(155,"img/cherries.png"),(159,"img/cherries.png"),
(4,"img/corn.png"),(73,"img/corn.png"),
(5,"img/strawberry.png"),
(6,"img/potato.png"),(82,"img/potato.png"),
(7,"img/watermelon.png"),
(8,"img/celery.png"),
(9,"img/lettuce.png"),
(10,"img/radish.png"),(35,"img/radish.png"),(93,"img/radish.png"),
(11,"img/salad.png"),(19,"img/salad.png"),
(12,"img/parsley.png"),(102,"img/parsley.png"),
(13,"img/green-beans.png"),(136,"img/green-beans.png"),(52,"img/beans.png"),
(14,"img/peas.png"),(43,"img/peas.png"),(83,"img/peas.png"),(100,"img/peas.png"),(108,"img/peas.png"),(166,"img/peas.png"),
(16,"img/pear.png"),(131,"img/pear.png"),(140,"img/pear.png"),(185,"img/pear.png"),
(17,"img/grain.png"),(90,"img/grain.png"),(125,"img/grain.png"),(172,"img/grain.png"),
(18,"img/tomato.png"),
(20,"img/flower.png"),(60,"img/flower.png"),(96,"img/flower.png"),(127,"img/flower.png"),(148,"img/flower.png"),(154,"img/flower.png"),(186,"img/flower.png"),(188,"img/flower.png"),
(21,"img/grass.png"),(25,"img/grass.png"),(40,"img/grass.png"),(31,"img/grass.png"),(32,"img/grass.png"),(36,"img/grass.png"),(48,"img/grass.png"),(37,"img/grass.png"),(51,"img/grass.png"),
(22,"img/soy.png"),
(23,"img/chilli-pepper.png"),
(24,"img/aubergine.png"),(180,"img/aubergine.png"),
(26,"img/apricot.png"),
(27,"img/beet.png"),
(28,"img/rapeseed.png"),(183,"img/rapeseed.png"),
(29,"img/linen.png"),(45,"img/linen.png"),(68,"img/linen.png"),(75,"img/linen.png"),
(30,"img/mustard.png"),(138,"img/mustard.png"),
(33,"img/pickle.png"),
(34,"img/onion.png"),(123,"img/onion.png"),
(38,"img/chard.png"),
(39,"img/courgette.png"),
(41,"img/raspberries.png"),
(42,"img/apple.png"),(76,"img/apple.png"),(118,"img/apple.png"),
(44,"img/carrot.png"),
(49,"img/olives.png"),
(50,"img/grain.png"),
(53,"img/chestnut.png"),
(54,"img/almond.png"),
(55,"img/garlic.png"),
(56,"img/melon.png"),
(57,"img/oat.png"),
(58,"img/grass.png"),
(59,"img/flower.png"),(62,"img/flower.png"),(137,"img/flower.png"),(133,"img/flower.png"),(105,"img/flower.png"),(65,"img/flower.png"),(85,"img/flower.png"),(72,"img/flower.png"),(79,"img/flower.png"),(80,"img/flower.png"),(99,"img/flower.png"),(94,"img/flower.png"),(184,"img/flower.png"),(190,"img/flower.png"),
(61,"img/cabbage.png"),
(63,"img/grass.png"),
(64,"img/sunflower.png"),
(66,"img/plum.png"),
(67,"img/grass.png"),
(69,"img/beet.png"),
(70,"img/cabbage.png"),
(71,"img/oat.png"),
(74,"img/peach.png"),
(77,"img/grass.png"),
(78,"img/grass.png"),
(81,"img/salad.png"),
(84,"img/cassis.png"),
(86,"img/lentil.png"),
(87,"img/plum.png"),
(88,"img/grass.png"),
(89,"img/cabbage.png"),
(91,"img/spinach.png"),
(92,"img/cabbage.png"),
(95,"img/rice.png"),
(97,"img/grass.png"),
(98,"img/leek.png"),
(101,"img/asparagus.png"),
(103,"img/cabbage.png"),
(104,"img/mandarin.png"),
(106,"img/plum.png"),
(109,"img/grass.png"),(110,"img/grass.png"),(111,"img/grass.png"),(112,"img/grass.png"),(113,"img/grass.png"),
(114,"img/fennel.png"),
(115,"img/butternut-squash.png"),
(116,"img/hazelnut.png"),
(117,"img/walnut.png"),
(119,"img/oat.png"),
(120,"img/chard.png"),
(121,"img/turnip.png"),
(122,"img/artichoke.png"),
(124,"img/pumkin.png"),
(126,"img/grass.png"),
(128,"img/beet.png"),
(129,"img/quince.png"),
(132,"img/shallot.png"),
(134,"img/mandarin.png"),
(135,"img/mandarin.png"),
(139,"img/walnut.png"),
(141,"img/cabbage.png"),
(142,"img/quince.png"),
(143,"img/cabbage.png"),
(144,"img/parsley.png"),
(145,"img/chives.png"),
(146,"img/lemon.png"),
(147,"img/plum.png"),
(149,"img/cassis.png"),
(150,"img/weed.png"),
(152,"img/grass.png"),
(153,"img/cabbage.png"),
(156,"img/lemon.png"),
(157,"img/cabbage.png"),
(158,"img/grass.png"),
(160,"img/plum.png"),
(161,"img/plum.png"),
(162,"img/plum.png"),
(163,"img/plum.png"),
(164,"img/garlic.png"),
(165,"img/lemon.png"),
(167,"img/oat.png"),
(168,"img/grass.png"),
(169,"img/cabbage.png"),
(170,"img/lemon.png"),
(171,"img/oat.png"),
(173,"img/apricot.png"),
(174,"img/plum.png"),
(175,"img/mandarin.png"),
(176,"img/mandarin.png"),
(177,"img/grass.png"),
(178,"img/lemon.png"),
(179,"img/mandarin.png"),
(181,"img/grass.png"),
(182,"img/lemon.png"),
(187,"img/cabbage.png"),
(189,"img/weed.png"),
(191,"img/lemon.png"),
(192,"img/lemon.png"),
(193,"img/plum.png");');

mysqli_query($connexion,
"INSERT INTO `Récolter` (`id_parcelle`, `NumRang`, `id_variété`, `Période_début`, `Période_fin`, `quantité`, `qualité_gustative`, `commentaire`, `type_mise_en_place`) VALUES
(1, 1, 6952, '2021-12-12 13:23:46', '2022-01-12 13:23:46', NULL, NULL, NULL, NULL),
(1, 2, 3432, '2021-12-12 13:23:46', '2022-01-12 13:23:46', NULL, NULL, NULL, NULL),
(1, 3, 6134, '2021-12-12 13:23:46', '2022-01-12 13:23:46', NULL, NULL, NULL, NULL),
(1, 4, 5379, '2021-12-12 13:23:46', '2022-01-12 13:23:46', NULL, NULL, NULL, NULL),
(2, 1, 2960, '2021-12-12 13:23:54', '2022-01-12 13:23:54', NULL, NULL, NULL, NULL),
(2, 1, 4907, '2021-12-12 13:23:54', '2022-01-12 13:23:54', NULL, NULL, NULL, NULL),
(2, 2, 2729, '2021-12-12 13:23:54', '2022-01-12 13:23:54', NULL, NULL, NULL, NULL),
(2, 3, 7725, '2021-12-12 13:23:54', '2022-01-12 13:23:54', NULL, NULL, NULL, NULL),
(3, 1, 491, '2021-12-12 13:25:31', '2022-01-12 13:25:31', NULL, NULL, NULL, NULL),
(3, 2, 3937, '2021-12-12 13:25:31', '2022-01-12 13:25:31', NULL, NULL, NULL, NULL),
(3, 2, 7400, '2021-12-12 13:25:31', '2022-01-12 13:25:31', NULL, NULL, NULL, NULL),
(4, 1, 4886, '2021-12-13 16:04:49', '2022-01-13 16:04:49', NULL, NULL, NULL, NULL),
(4, 1, 4933, '2021-12-13 16:04:49', '2022-01-13 16:04:49', NULL, NULL, NULL, NULL),
(4, 2, 4696, '2021-12-13 16:04:49', '2022-01-13 16:04:49', NULL, NULL, NULL, NULL),
(4, 2, 8087, '2021-12-13 16:04:49', '2022-01-13 16:04:49', NULL, NULL, NULL, NULL),
(4, 3, 3796, '2021-12-13 16:04:49', '2022-01-13 16:04:49', NULL, NULL, NULL, NULL),
(4, 4, 970, '2021-12-13 16:04:49', '2022-01-13 16:04:49', NULL, NULL, NULL, NULL),
(5, 1, 6500, '2021-12-13 16:16:15', '2022-01-13 16:16:15', NULL, NULL, NULL, NULL),
(5, 1, 7643, '2021-12-13 16:16:15', '2022-01-13 16:16:15', NULL, NULL, NULL, NULL),
(5, 2, 1988, '2021-12-13 16:16:15', '2022-01-13 16:16:15', NULL, NULL, NULL, NULL),
(5, 2, 8569, '2021-12-13 16:16:15', '2022-01-13 16:16:15', NULL, NULL, NULL, NULL),
(5, 3, 431, '2021-12-13 16:16:15', '2022-01-13 16:16:15', NULL, NULL, NULL, NULL),
(5, 4, 4463, '2021-12-13 16:16:15', '2022-01-13 16:16:15', NULL, NULL, NULL, NULL),
(5, 5, 2288, '2021-12-13 16:16:15', '2022-01-13 16:16:15', NULL, NULL, NULL, NULL),
(6, 1, 3419, '2021-12-13 16:18:42', '2022-01-13 16:18:42', NULL, NULL, NULL, NULL),
(6, 1, 8801, '2021-12-13 16:18:42', '2022-01-13 16:18:42', NULL, NULL, NULL, NULL),
(6, 2, 1164, '2021-12-13 16:18:42', '2022-01-13 16:18:42', NULL, NULL, NULL, NULL),
(6, 2, 4568, '2021-12-13 16:18:42', '2022-01-13 16:18:42', NULL, NULL, NULL, NULL),
(6, 3, 4607, '2021-12-13 16:18:42', '2022-01-13 16:18:42', NULL, NULL, NULL, NULL),
(6, 4, 8654, '2021-12-13 16:18:42', '2022-01-13 16:18:42', NULL, NULL, NULL, NULL);");

mysqli_query($connexion,"
INSERT INTO `Association_plante` (`id_plante`, `id_plante_1`) VALUES
(3, 62),
(4, 14),
(4, 131),
(6, 27),
(13, 42),
(15, 21),
(18, 23),
(21, 4),
(22, 17),
(24, 1),
(53, 1),
(56, 1),
(64, 1),
(64, 79),
(66, 4),
(69, 13),
(70, 16),
(98, 45);");
?>
